package com.example.indira.chocotestproject.city;

import com.example.indira.chocotestproject.model.City;

import java.util.List;

public interface ICityView {
    void displayCities(List<City> cities);
}
