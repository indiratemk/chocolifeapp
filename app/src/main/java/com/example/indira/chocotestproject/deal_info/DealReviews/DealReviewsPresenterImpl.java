package com.example.indira.chocotestproject.deal_info.DealReviews;

import android.util.Log;

import com.example.indira.chocotestproject.model.DealReview;
import com.example.indira.chocotestproject.model.JsonResponse;
import com.example.indira.chocotestproject.service.ServiceWrapper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.constraint.Constraints.TAG;

public class DealReviewsPresenterImpl implements IDealReviewsPresenter {

    private IDealReviewsView view;
    private ServiceWrapper serviceInstance;

    public DealReviewsPresenterImpl(DealReviewsFragment view) {
        this.view = view;
    }

    @Override
    public void getReviews(int dealId, int page, int userMark) {
        serviceInstance = new ServiceWrapper();

        Call<JsonResponse<List<DealReview>>> responseCall =
                serviceInstance.getDealReviewResponse(dealId, page, userMark);

        responseCall.enqueue(new Callback<JsonResponse<List<DealReview>>>() {
            @Override
            public void onResponse(Call<JsonResponse<List<DealReview>>> call, Response<JsonResponse<List<DealReview>>> response) {
                if (response.body() != null && response.isSuccessful()) {
                    view.displayReviews(response.body().getResponse());
                }
            }

            @Override
            public void onFailure(Call<JsonResponse<List<DealReview>>> call, Throwable t) {
                view.showError();
            }
        });
    }
}
