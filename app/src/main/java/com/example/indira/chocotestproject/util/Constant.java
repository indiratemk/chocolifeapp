package com.example.indira.chocotestproject.util;

public class Constant {
    public static final String BASE_URL = "https://chocolife.me/mobileapi/";

    public static final int DEFAULT_CITY_ID = 1;
    public static final int DEFAULT_CITY_INDEX = 0;
    public static final int DEFAULT_CATEGORY_ID = 1;
    public static final int INITIAL_PAGE = 1;
    public static final int AMOUNT_ITEMS_IN_PAGE = 20;

    public static final String HTML_START = "<html><head><link href=\"style.css\" type=\"text/css\" rel=\"stylesheet\"/></head><body>";
    public static final String HTML_END = "</body></html>";
    public static final String CSS_PATH = "file:///android_asset/";
    public static final String DEFAULT_CITY_TITLE = "Алматы";

}
