package com.example.indira.chocotestproject.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Category {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("deals_number")
    @Expose
    private int dealsNumber;

    @SerializedName("sub_categories")
    @Expose
    private List<Category> subCategories;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getDealsNumber() {
        return dealsNumber;
    }

    public List<Category> getSubCategories() {
        return subCategories;
    }
}
