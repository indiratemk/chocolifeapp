package com.example.indira.chocotestproject.deal_info.DealReviews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.indira.chocotestproject.R;
import com.example.indira.chocotestproject.model.DealInfo;
import com.example.indira.chocotestproject.model.RateStatistics;

public class DealReviewStatisticsHolder extends RecyclerView.ViewHolder {
    private static final String TAG = "DealReviewStatisticsHol";

    private TextView partnerReviewsRate;
    private TextView overallReviewsRate;
    private TextView overallReviewsNumber;
    private ProgressBar fiveStarsProgressBar;
    private TextView fiveStarsCount;
    private ProgressBar fourStarsProgressBar;
    private TextView fourStarsCount;
    private ProgressBar threeStarsProgressBar;
    private TextView threeStarsCount;
    private ProgressBar twoStarsProgressBar;
    private TextView twoStarsCount;
    private ProgressBar oneStarProgressBar;
    private TextView oneStarCount;
    private Spinner reviewsSortSpinner;

    public DealReviewStatisticsHolder(@NonNull View itemView) {
        super(itemView);

        partnerReviewsRate = itemView.findViewById(R.id.partner_reviews_rate);
        overallReviewsRate = itemView.findViewById(R.id.overall_reviews_rate);
        overallReviewsNumber = itemView.findViewById(R.id.overall_reviews_number);
        fiveStarsProgressBar = itemView.findViewById(R.id.five_stars_progress_bar);
        fiveStarsCount = itemView.findViewById(R.id.five_stars_count);
        fourStarsProgressBar = itemView.findViewById(R.id.four_stars_progress_bar);
        fourStarsCount = itemView.findViewById(R.id.four_stars_count);
        threeStarsProgressBar = itemView.findViewById(R.id.three_stars_progress_bar);
        threeStarsCount = itemView.findViewById(R.id.three_stars_count);
        twoStarsProgressBar = itemView.findViewById(R.id.two_stars_progress_bar);
        twoStarsCount = itemView.findViewById(R.id.two_stars_count);
        oneStarProgressBar = itemView.findViewById(R.id.one_star_progress_bar);
        oneStarCount = itemView.findViewById(R.id.one_star_count);
        reviewsSortSpinner = itemView.findViewById(R.id.reviews_sort_spinner);

    }

    public void bindTo(Context context, final DealInfo dealInfo,
                       final DealReviewsAdapter.OnSortBySelectListener onSortBySelectListener) {
        RateStatistics rateStatistics = dealInfo.getStatistics();
        int reviewsNumber = rateStatistics.getReviewsNumber();
        partnerReviewsRate.setText(String.valueOf(dealInfo.getPartner().getPartnerRate()));
        overallReviewsRate.setText(String.valueOf(rateStatistics.getReviewsRate()));
        overallReviewsNumber.setText(context.getString(R.string.deal_info_reviews_number_text,
                reviewsNumber));

        int fiveStarsNumber = rateStatistics.getFiveStarsCount();
        int fourStarsNumber = rateStatistics.getFourStarsCount();
        int threeStarsNumber = rateStatistics.getThreeStarsCount();
        int twoStarsNumber = rateStatistics.getTwoStarsCount();
        int oneStarNumber = rateStatistics.getOneStarCount();

        fiveStarsCount.setText(String.valueOf(fiveStarsNumber));
        fourStarsCount.setText(String.valueOf(fourStarsNumber));
        threeStarsCount.setText(String.valueOf(threeStarsNumber));
        twoStarsCount.setText(String.valueOf(twoStarsNumber));
        oneStarCount.setText(String.valueOf(oneStarNumber));

        if(reviewsNumber != 0) {
            fiveStarsProgressBar.setProgress(fiveStarsNumber * 100 / reviewsNumber);
            fourStarsProgressBar.setProgress(fourStarsNumber * 100 / reviewsNumber);
            threeStarsProgressBar.setProgress(threeStarsNumber * 100 / reviewsNumber);
            twoStarsProgressBar.setProgress(twoStarsNumber * 100 / reviewsNumber);
            oneStarProgressBar.setProgress(oneStarNumber * 100 / reviewsNumber);
        } else {
            overallReviewsRate.setText(String.valueOf((int)rateStatistics.getReviewsRate()));
        }

        final ArrayAdapter<CharSequence> sortAdapter = ArrayAdapter.createFromResource(itemView.getContext(),
                R.array.reviews_sort, android.R.layout.simple_spinner_item);
        sortAdapter.setDropDownViewResource(R.layout.reviews_spinner_item);
        reviewsSortSpinner.setAdapter(sortAdapter);
        reviewsSortSpinner.setSelection(dealInfo.getSpinnerPosition());
        reviewsSortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int userMark = position;
                if (position > 0) {
                    userMark = sortAdapter.getCount() - position;
                }
                dealInfo.setSpinnerPosition(position);
                onSortBySelectListener.onSortBySelect(userMark);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

}
