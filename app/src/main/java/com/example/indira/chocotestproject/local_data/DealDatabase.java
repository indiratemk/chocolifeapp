package com.example.indira.chocotestproject.local_data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.indira.chocotestproject.model.Deal;

@Database(entities = {Deal.class}, version = 1, exportSchema = false)
public abstract class DealDatabase extends RoomDatabase {
    private static DealDatabase dbInstance;

    public abstract DealDao dealDao();

    public static synchronized DealDatabase getInstance(Context context) {
        if (dbInstance == null) {
            dbInstance = Room.databaseBuilder(context.getApplicationContext(),
                    DealDatabase.class, "deal_database")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return dbInstance;
    }
}
