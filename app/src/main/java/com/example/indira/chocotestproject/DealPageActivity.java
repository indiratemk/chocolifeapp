package com.example.indira.chocotestproject;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.example.indira.chocotestproject.deal.DealAdapter;

public class DealPageActivity extends AppCompatActivity {

    private WebView webView;
    private ProgressBar progressBar;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deal_page);

        Intent intent = getIntent();
        String url = intent.getStringExtra(DealAdapter.EXTRA_DEAL_LINK);

        progressBar = findViewById(R.id.deal_page_progress_bar);
        progressBar.setMax(100);

        webView = findViewById(R.id.deal_web_view);
        webView.getSettings().setJavaScriptEnabled(true);

        webView.setWebChromeClient(new WebChromeClient() {
           public void onProgressChanged(WebView webView, int newProgress) {
               if (newProgress == 100) {
                   progressBar.setVisibility(View.GONE);
               } else {
                   progressBar.setVisibility(View.VISIBLE);
                   progressBar.setProgress(newProgress);
               }
           }

           @Override
           public void onReceivedTitle(WebView view, String title) {
               getSupportActionBar().setSubtitle(title);
               getSupportActionBar().setTitle(R.string.child_activity_title);
           }
       });

        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(url);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.deal_info_menu, menu );
        return true;
    }
}
