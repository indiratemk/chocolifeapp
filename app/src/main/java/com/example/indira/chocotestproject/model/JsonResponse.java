package com.example.indira.chocotestproject.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JsonResponse<T> {

    @SerializedName("result")
    @Expose
    private T response;

    public T getResponse() {
        return response;
    }
}
