package com.example.indira.chocotestproject;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.indira.chocotestproject.deal.DealAdapter;
import com.example.indira.chocotestproject.deal_info.DealConditionsFragment;
import com.example.indira.chocotestproject.deal_info.DealFeaturesFragment;
import com.example.indira.chocotestproject.deal_info.IDealInfoPresenter;
import com.example.indira.chocotestproject.deal_info.DealInfoPresenterImpl;
import com.example.indira.chocotestproject.deal_info.IDealInfoView;
import com.example.indira.chocotestproject.deal_info.DealInformationFragment;
import com.example.indira.chocotestproject.deal_info.DealPagerAdapter;
import com.example.indira.chocotestproject.deal_info.DealComments.DealCommentsFragment;
import com.example.indira.chocotestproject.deal_info.DealReviews.DealReviewsFragment;
import com.example.indira.chocotestproject.model.DealInfo;

public class DealInfoActivity extends AppCompatActivity implements IDealInfoView {
    public static final String ARG_DEAL_INFO = "deal_info";
    private static final String TAG = "DealInfoActivity";

    private IDealInfoPresenter dealInfoPresenter;

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private ProgressBar dealInfoLoading;


    private int dealId;
    private DealPagerAdapter pageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deal_info);

        dealInfoPresenter = new DealInfoPresenterImpl(this);

        Intent intent = getIntent();
        dealId = intent.getIntExtra(DealAdapter.EXTRA_DEAL_ID, -1);

        Log.d(TAG, "onCreate: deal id: " + dealId);

        tabLayout = findViewById(R.id.deal_info_tab_layout);
        viewPager = findViewById(R.id.deal_info_pager);
        dealInfoLoading = findViewById(R.id.deal_info_loading);

        dealInfoPresenter.getDealInfo(dealId);

        pageAdapter = new DealPagerAdapter(getSupportFragmentManager());

    }

    private void setupViewPage(DealInfo dealInfo) {
        dealInfoLoading.setVisibility(View.GONE);
        viewPager.setVisibility(View.VISIBLE);
        tabLayout.setVisibility(View.VISIBLE);

        pageAdapter.addFragment(DealInformationFragment.getInstance(dealInfo), getText(R.string.tab_information).toString());
        pageAdapter.addFragment(DealConditionsFragment.getInstance(dealInfo), getText(R.string.tab_conditions).toString());
        pageAdapter.addFragment(DealFeaturesFragment.getInstance(dealInfo), getText(R.string.tab_features).toString());
        pageAdapter.addFragment(DealReviewsFragment.getInstance(dealInfo), getText(R.string.tab_reviews).toString());
        pageAdapter.addFragment(DealCommentsFragment
                .getInstance(dealId, dealInfo.getQuestionNumber()), getText(R.string.tab_questions).toString());

        viewPager.setOffscreenPageLimit(pageAdapter.getCount());
        viewPager.setAdapter(pageAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.deal_info_menu, menu );
        return true;
    }

    @Override
    public void displayDealInfo(DealInfo dealInfo) {
        getSupportActionBar().setTitle(dealInfo.getShortTitle());
        setupViewPage(dealInfo);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void showError() {
        Toast.makeText(this,
                getString(R.string.internet_connection_error), Toast.LENGTH_SHORT).show();
    }

}
