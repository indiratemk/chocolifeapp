package com.example.indira.chocotestproject.deal_info.DealReviews;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.indira.chocotestproject.R;
import com.example.indira.chocotestproject.model.DealReview;
import com.example.indira.chocotestproject.model.User;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class DealReviewsHolder extends RecyclerView.ViewHolder {
    private static final String TAG = "DealReviewsHolder";

    private CircleImageView userAvatar;
    private TextView userName;
    private TextView reviewDate;
    private LinearLayout recommendContainer;
    private TextView userReview;
    private TextView reviewRating;
    private LinearLayout reviewRatingContainer;

    public DealReviewsHolder(@NonNull View itemView) {
        super(itemView);

        userAvatar = itemView.findViewById(R.id.review_user_avatar);
        userName = itemView.findViewById(R.id.review_user_name);
        reviewDate = itemView.findViewById(R.id.review_date);
        recommendContainer = itemView.findViewById(R.id.recommend_deal_container);
        userReview = itemView.findViewById(R.id.review_user_text);
        reviewRating = itemView.findViewById(R.id.review_rating);
        reviewRatingContainer = itemView.findViewById(R.id.review_rating_container);
    }

    public void bindTo(Context context, DealReview dealReview) {
        User user = dealReview.getUser();
        Picasso.get().load(user.getAvatarUrl()).into(userAvatar);
        if (user.getName() == null || user.getName().isEmpty()) {
            userName.setText(context.getString(R.string.no_name_text));
        } else {
            userName.setText(user.getName());
        }

        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date commentDate = format.parse(dealReview.getDate());
            SimpleDateFormat updatedFormat = new SimpleDateFormat("dd.MM.yy");
            reviewDate.setText(updatedFormat.format(commentDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (dealReview.isRecommend()) {
            recommendContainer.setVisibility(View.VISIBLE);
        }
        userReview.setText(dealReview.getUserSimpleComment());
        reviewRating.setText(String.valueOf(dealReview.getMark()));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            switch (dealReview.getMark()) {
                case 5:
                    reviewRatingContainer.setBackgroundTintList(
                            context.getResources().getColorStateList(R.color.colorGreen));
                    break;
                case 4:
                    reviewRatingContainer.setBackgroundTintList
                            (context.getResources().getColorStateList(R.color.colorLightGreen));
                    break;
                case 3:
                    reviewRatingContainer.setBackgroundTintList
                            (context.getResources().getColorStateList(R.color.colorOrange));
                    break;
                case 2:
                    reviewRatingContainer.setBackgroundTintList
                            (context.getResources().getColorStateList(R.color.colorYellow));
                    break;
                case 1:
                    reviewRatingContainer.setBackgroundTintList
                            (context.getResources().getColorStateList(R.color.colorGray));
                    break;
            }
        }
    }
}
