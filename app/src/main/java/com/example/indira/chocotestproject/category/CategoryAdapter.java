package com.example.indira.chocotestproject.category;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.indira.chocotestproject.R;
import com.example.indira.chocotestproject.model.Category;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryHolder> {
    private static final String TAG = "CategoryAdapter";

    private Context context;
    private OnCategoryClickListener onCategoryClickListener;
    private List<Category> categories;
    private int selectedCategoryIndex = 0;

    public CategoryAdapter(List<Category> categories, Context context) {
        this.categories = categories;
        this.context = context;
    }

    @NonNull
    @Override
    public CategoryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.category_item, viewGroup, false);
        return new CategoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CategoryHolder categoryHolder, final int i) {
        final Category category = categories.get(i);
        categoryHolder.categoryTitle.setText(category.getTitle());
        categoryHolder.dealsAmount.setText(String.valueOf(category.getDealsNumber()));

        if (!category.getSubCategories().isEmpty()) {
            categoryHolder.showMoreIcon.setVisibility(View.VISIBLE);
            categoryHolder.dealsAmount.setVisibility(View.GONE);
            SubCategoryAdapter subCategoryAdapter =
                    new SubCategoryAdapter(context, category.getSubCategories());

            subCategoryAdapter.setOnCategoryClickListener(new OnCategoryClickListener() {
                @Override
                public void onCategoryClick(int categoryId, int dealsNumber) {
                    onCategoryClickListener.onCategoryClick(categoryId, dealsNumber);
                }
            });

            categoryHolder.subCategoryRecyclerView.setAdapter(subCategoryAdapter);
        } else {
            categoryHolder.showMoreIcon.setVisibility(View.GONE);
            categoryHolder.dealsAmount.setVisibility(View.VISIBLE);
        }

        if (selectedCategoryIndex != i) {
            categoryHolder.categoryTitle.setBackgroundResource(R.drawable.category_default_bg);
        } else {
            categoryHolder.categoryTitle.setBackgroundResource(R.drawable.category_item_border);
        }

    }

    @Override
    public int getItemCount() {
        if (categories == null) {
            return 0;
        }
        return categories.size();
    }


    class CategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView categoryTitle;
        private TextView dealsAmount;
        private ImageView showMoreIcon;
        private ImageView hideMoreIcon;

        private RecyclerView subCategoryRecyclerView;

        private CategoryHolder(@NonNull View itemView) {
            super(itemView);
            categoryTitle = itemView.findViewById(R.id.category_title_text_view);
            dealsAmount = itemView.findViewById(R.id.deals_amount_text_view);
            showMoreIcon = itemView.findViewById(R.id.show_more_image_view);
            hideMoreIcon = itemView.findViewById(R.id.hide_more_image_view);
            subCategoryRecyclerView = itemView.findViewById(R.id.subcategory_recycler_view);
            subCategoryRecyclerView.setLayoutManager(new LinearLayoutManager(itemView.getContext()));

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            categoryTitle.setBackgroundResource(R.drawable.category_item_border);
            int oldSelected = selectedCategoryIndex;
            selectedCategoryIndex = getAdapterPosition();
            notifyItemChanged(oldSelected);
            Category category = categories.get(getAdapterPosition());
            if (category.getSubCategories().isEmpty()) {
                onCategoryClickListener.onCategoryClick(category.getId(), category.getDealsNumber());
            } else {
                if (subCategoryRecyclerView.getVisibility() == View.GONE) {
                    subCategoryRecyclerView.setVisibility(View.VISIBLE);
                    showMoreIcon.setVisibility(View.GONE);
                    hideMoreIcon.setVisibility(View.VISIBLE);
                } else {
                    subCategoryRecyclerView.setVisibility(View.GONE);
                    showMoreIcon.setVisibility(View.VISIBLE);
                    hideMoreIcon.setVisibility(View.GONE);
                }
            }
        }
    }

    public void setOnCategoryClickListener(OnCategoryClickListener onCategoryClickListener) {
        this.onCategoryClickListener = onCategoryClickListener;
    }
}
