package com.example.indira.chocotestproject.local_data;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.indira.chocotestproject.model.Deal;

import java.util.List;

@Dao
public interface DealDao {

    @Insert
    void insertDeal(Deal deal);

    @Delete
    void deleteDeal(Deal deal);

    @Query("DELETE FROM deal_table")
    void deleteAllDeals();

    @Query("SELECT * FROM deal_table")
    LiveData<List<Deal>> getAllDeals();
}
