package com.example.indira.chocotestproject.deal_info;

import android.util.Log;

import com.example.indira.chocotestproject.DealInfoActivity;
import com.example.indira.chocotestproject.model.DealInfo;
import com.example.indira.chocotestproject.model.JsonResponse;
import com.example.indira.chocotestproject.service.ServiceWrapper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DealInfoPresenterImpl implements IDealInfoPresenter {
    private static final String TAG = "DealInfoPresenterImpl";

    private IDealInfoView view;
    private ServiceWrapper serviceInstance;

    public DealInfoPresenterImpl(DealInfoActivity view) {
        this.view = view;
    }

    @Override
    public void getDealInfo(int dealId) {
        serviceInstance = new ServiceWrapper();

        Call<JsonResponse<DealInfo>> responseCall = serviceInstance.getDealInfoResponse(dealId);


        responseCall.enqueue(new Callback<JsonResponse<DealInfo>>() {
            @Override
            public void onResponse(Call<JsonResponse<DealInfo>> call, Response<JsonResponse<DealInfo>> response) {
                if (response.body() != null && response.isSuccessful()) {

                    view.displayDealInfo(response.body().getResponse());
                }
            }

            @Override
            public void onFailure(Call<JsonResponse<DealInfo>> call, Throwable t) {
                view.showError();
                Log.d(TAG, "onFailure: dealInfoPresenter: " + t.getMessage());
            }
        });
    }
}
