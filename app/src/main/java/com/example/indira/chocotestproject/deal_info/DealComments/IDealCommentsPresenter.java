package com.example.indira.chocotestproject.deal_info.DealComments;

public interface IDealCommentsPresenter {
    void getComments(int dealId, int page);
}
