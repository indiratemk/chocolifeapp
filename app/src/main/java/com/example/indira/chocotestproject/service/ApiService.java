package com.example.indira.chocotestproject.service;


import com.example.indira.chocotestproject.model.Category;
import com.example.indira.chocotestproject.model.City;
import com.example.indira.chocotestproject.model.Deal;
import com.example.indira.chocotestproject.model.DealComment;
import com.example.indira.chocotestproject.model.DealInfo;
import com.example.indira.chocotestproject.model.DealReview;
import com.example.indira.chocotestproject.model.JsonResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {
    String CITIES = "v3/towns";
    String CATEGORIES = "v3_3/categories";
    String DEALS = "v3_3/deals";
    String DEAL_INFO = "v3/deal";
    String DEAL_COMMENTS = "v3/deal/comments";
    String DEAL_REVIEWS = "v3/deal/reviews";

    String CITY_QUERY = "town_id";
    String CATEGORY_QUERY = "category_id";
    String PAGE_QUERY = "page";
    String DEAL_QUERY = "deal_id";
    String USER_MARK_QUERY = "user_mark";
    String SORTING_QUERY = "sort";
    String SEARCH_QUERY = "search_text";

    @GET(CITIES)
    Call<JsonResponse<List<City>>> getCitiesResponse();

    @GET(CATEGORIES)
    Call<JsonResponse<List<Category>>> getCategoriesResponse(@Query(CITY_QUERY) int cityId);


    @GET(DEAL_INFO)
    Call<JsonResponse<DealInfo>> getDealInfoResponse(@Query(DEAL_QUERY) int dealId);

    @GET(DEAL_COMMENTS)
    Call<JsonResponse<List<DealComment>>> getDealCommentResponse(@Query(DEAL_QUERY) int dealId,
                                                           @Query(PAGE_QUERY) int page);

    @GET(DEAL_REVIEWS)
    Call<JsonResponse<List<DealReview>>> getDealReviewResponse(@Query(DEAL_QUERY) int dealId,
                                                               @Query(PAGE_QUERY) int page,
                                                               @Query(USER_MARK_QUERY) int userMark);


    @GET(DEALS)
    Call<JsonResponse<List<Deal>>> getDealsResponse(@Query(CITY_QUERY) int cityId,
                                                    @Query(CATEGORY_QUERY) int categoryId,
                                                    @Query(SORTING_QUERY) String sortingParameter,
                                                    @Query(PAGE_QUERY) int page);

    @GET(DEALS)
    Call<JsonResponse<List<Deal>>> getSearchDealsResponse(@Query(CITY_QUERY) int cityId,
                                                          @Query(SEARCH_QUERY) String searchParameter,
                                                          @Query(PAGE_QUERY) int page);
}
