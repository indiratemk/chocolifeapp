package com.example.indira.chocotestproject;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.indira.chocotestproject.category.CategoryAdapter;
import com.example.indira.chocotestproject.category.CategoryPresenterImpl;
import com.example.indira.chocotestproject.category.ICategoryPresenter;
import com.example.indira.chocotestproject.category.ICategoryView;
import com.example.indira.chocotestproject.category.OnCategoryClickListener;
import com.example.indira.chocotestproject.city.CitiesDialog;
import com.example.indira.chocotestproject.city.CityPresenterImpl;
import com.example.indira.chocotestproject.city.ICityPresenter;
import com.example.indira.chocotestproject.city.ICityView;
import com.example.indira.chocotestproject.deal.DealAdapter;
import com.example.indira.chocotestproject.deal.DealPresenterImpl;
import com.example.indira.chocotestproject.deal.IDealPresenter;
import com.example.indira.chocotestproject.deal.IDealView;
import com.example.indira.chocotestproject.deal.SortingDialog;
import com.example.indira.chocotestproject.local_data.DealViewModel;
import com.example.indira.chocotestproject.model.Category;
import com.example.indira.chocotestproject.model.City;
import com.example.indira.chocotestproject.model.Deal;
import com.example.indira.chocotestproject.util.Constant;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements ICityView,
        CitiesDialog.OnCityDialogListener, SortingDialog.OnSortingDialogListener,
        ICategoryView, IDealView {

    private static final String TAG = "MainActivity";
    private static final String SELECTED_CITY_ID_KEY = "selected_city_id";
    private static final String SELECTED_CITY_TITLE_KEY = "selected_city_title";
    private static final String SELECTED_CITY_INDEX = "selected_city_index";

    private boolean isFirstSearch = false;
    private String searchParameter;

    private int checkedItemId = R.id.radio_by_default;
    private String sortingParameter;

    private int dealsAmount;
    private int current_page = 1;
    private int pageAmount;
    private List<Deal> allDeals = new ArrayList<>();

    private int selectedCategoryId;
    private int selectedCityId;
    private int selectedCityIndex;

    private RecyclerView categoryRecyclerView;
    private CategoryAdapter categoryAdapter;

    private RecyclerView dealsRecyclerView;
    private DealAdapter dealAdapter;

    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private TextView selectCityTextView;
    private ConstraintLayout changeCityLayout;

    private CitiesDialog citiesDialog;
    private SortingDialog sortingDialog;

    private ICityPresenter cityPresenter;
    private ICategoryPresenter categoryPresenter;
    private IDealPresenter dealPresenter;
    private DealViewModel dealViewModel;

    private SharedPreferences sharedPreferences;
    private String sharedPrefFile = "com.example.indira.chocoproject";

    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar = findViewById(R.id.loading);
        init();
    }

    public void init() {
        sharedPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);

        selectCityTextView = findViewById(R.id.choose_city_text_view);
        selectedCityId = sharedPreferences.getInt(SELECTED_CITY_ID_KEY, Constant.DEFAULT_CITY_ID);
        selectCityTextView.setText(sharedPreferences
                .getString(SELECTED_CITY_TITLE_KEY, Constant.DEFAULT_CITY_TITLE));
        selectedCityIndex = sharedPreferences.getInt(SELECTED_CITY_INDEX, Constant.DEFAULT_CITY_INDEX);


        cityPresenter = new CityPresenterImpl(this);
        categoryPresenter = new CategoryPresenterImpl(this);
        dealPresenter = new DealPresenterImpl(this);

        drawerLayout = findViewById(R.id.drawer_layout);


        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        categoryRecyclerView = findViewById(R.id.category_recycler_view);
        dealsRecyclerView = findViewById(R.id.deals_recycler_view);

        dealAdapter = new DealAdapter(this);
        dealsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        dealsRecyclerView.setAdapter(dealAdapter);


        cityPresenter.getCities();
        categoryPresenter.getCategoryMenuItems(selectedCityId);

        dealPresenter.getDeals(selectedCityId, Constant.DEFAULT_CATEGORY_ID,
                sortingParameter, Constant.INITIAL_PAGE);


        changeCityLayout = findViewById(R.id.change_city_layout);
        changeCityLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(Gravity.END);
                citiesDialog.show(getSupportFragmentManager(), "CitiesDialog");
            }
        });

        dealViewModel = ViewModelProviders.of(this).get(DealViewModel.class);
        LiveData<List<Deal>> dealsFromDatabase = dealViewModel.getAllDeals();
        dealsFromDatabase.observe(this, new Observer<List<Deal>>() {
            @Override
            public void onChanged(@Nullable List<Deal> deals) {
                dealAdapter.setDeals(deals);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search_action:
                SearchView searchView = (SearchView) item.getActionView();
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String s) {
                        isFirstSearch = true;
                        searchParameter = s;
                        dealPresenter.getSearchDeals(selectedCityId, searchParameter, Constant.INITIAL_PAGE);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String s) {
                        return false;
                    }
                });
                return true;
            case R.id.category_choose_action:
                drawerLayout.openDrawer(Gravity.END);
                return true;
            case R.id.sorting_action:
                sortingDialog = SortingDialog.newInstance(checkedItemId);
                sortingDialog.show(getSupportFragmentManager(), "Sorting Dialog");
                return true;
            case R.id.favorite_action:
                Toast.makeText(this, "Favorite item clicked", Toast.LENGTH_SHORT).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences.Editor preferencesEditor = sharedPreferences.edit();
        preferencesEditor.putInt(SELECTED_CITY_ID_KEY, selectedCityId);
        preferencesEditor.putString(SELECTED_CITY_TITLE_KEY,
                selectCityTextView.getText().toString());
        preferencesEditor.putInt(SELECTED_CITY_INDEX, selectedCityIndex);
        preferencesEditor.apply();
    }

    @Override
    public void displayCities(List<City> cities) {
        citiesDialog = CitiesDialog.newInstance(new ArrayList<>(cities), selectedCityIndex);
    }


    @Override
    public void displayError() {
        Toast.makeText(this,
                getString(R.string.internet_connection_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    public void displaySearchError() {
        Toast.makeText(this,
                getString(R.string.not_found_error), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void cityChanged(String cityTitle, int cityId, int cityIndex) {
        selectCityTextView.setText(cityTitle);
        selectedCityId = cityId;
        selectedCategoryId = Constant.DEFAULT_CATEGORY_ID;
        selectedCityIndex = cityIndex;
        categoryPresenter.getCategoryMenuItems(selectedCityId);
        dealsChanged();
    }

    @Override
    public void displayCategoryMenu(List<Category> categories) {
        selectedCategoryId = categories.get(0).getId();
        dealsAmount = categories.get(0).getDealsNumber();

        updatePageAmount();

        categoryAdapter = new CategoryAdapter(categories, this);
        categoryAdapter.setOnCategoryClickListener(new OnCategoryClickListener() {
            @Override
            public void onCategoryClick(int categoryId, int dealsNumber) {
                selectedCategoryId = categoryId;
                dealsAmount = dealsNumber;
                updatePageAmount();
                drawerLayout.closeDrawer(Gravity.END);
                dealsChanged();
            }
        });
        categoryRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        categoryRecyclerView.setAdapter(categoryAdapter);
    }

    private void dealsChanged() {
        dealAdapter.clearDealList();
        current_page = Constant.INITIAL_PAGE;
        dealPresenter.getDeals(selectedCityId, selectedCategoryId, sortingParameter, Constant.INITIAL_PAGE);
    }

    @Override
    public void displayDeals(List<Deal> deals) {
        hideLoading();
        if (current_page == 1) {
            if (selectedCategoryId == Constant.DEFAULT_CATEGORY_ID && sortingParameter == null) {
                dealViewModel.deleteAllDeals();
                for (Deal deal : deals) {
                    dealViewModel.insertDeal(deal);
                }
            } else {
                dealAdapter.setDeals(deals);
            }

        } else {
            if (dealsAmount == dealAdapter.getItemCount() || pageAmount < current_page) {
                current_page = Constant.INITIAL_PAGE;
                return;
            }
            dealAdapter.addDeals(deals);
        }
        dealAdapter.setOnEndReachedListener(new DealAdapter.OnEndReachedListener() {
            @Override
            public void onEndReached() {
                current_page += 1;
                dealPresenter.getDeals(selectedCityId, selectedCategoryId, sortingParameter, current_page);
            }
        });
    }

    @Override
    public void displaySearchDeals(List<Deal> deals) {
        hideLoading();
        if (isFirstSearch) {
            dealAdapter.clearDealList();
            isFirstSearch = false;
        }
        if (dealsAmount == dealAdapter.getItemCount() || pageAmount < current_page) {
            current_page = Constant.INITIAL_PAGE;
            return;
        }
        dealAdapter.addDeals(deals);
        if (dealAdapter.getDeals().isEmpty()) {
            displaySearchError();
        }
        dealAdapter.setOnEndReachedListener(new DealAdapter.OnEndReachedListener() {
            @Override
            public void onEndReached() {
                current_page += 1;
                dealPresenter.getSearchDeals(selectedCityId, searchParameter, current_page);
            }
        });
    }

    public void updatePageAmount() {
        pageAmount = dealsAmount/Constant.AMOUNT_ITEMS_IN_PAGE;
        if (dealsAmount/(double) Constant.AMOUNT_ITEMS_IN_PAGE > pageAmount) {
            pageAmount += 1;
        }
    }

    @Override
    public void sortingChanged(String sortingParameter, int checkedItemId) {
        this.checkedItemId = checkedItemId;
        this.sortingParameter = sortingParameter;
        dealAdapter.clearDealList();
        dealPresenter.getDeals(selectedCityId, selectedCategoryId, sortingParameter, Constant.INITIAL_PAGE);
        Toast.makeText(this, sortingParameter, Toast.LENGTH_SHORT).show();
        sortingDialog.dismiss();
    }
}
