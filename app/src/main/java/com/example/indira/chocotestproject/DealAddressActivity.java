package com.example.indira.chocotestproject;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.indira.chocotestproject.deal_info.DealInformationFragment;
import com.example.indira.chocotestproject.model.Place;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class DealAddressActivity extends AppCompatActivity {
    private static final String TAG = "DealAddressActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deal_address);

        List<Place> places = new ArrayList<>();
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            places = bundle.getParcelableArrayList(DealInformationFragment.EXTRA_PLACE);
        }

        final Map<String, Place> addressesMap = new LinkedHashMap<>();
        if (places != null) {
            for (Place place : places) {
                addressesMap.put(place.getAddress(), place);
            }
        }

        final List<String> addresses = new LinkedList<>(addressesMap.keySet());

        ListView addressesListView = findViewById(R.id.addresses_list_view);
        ArrayAdapter<String> addressesAdapter = new ArrayAdapter<>(this,
                R.layout.deal_address_item, addresses);
        addressesListView.setAdapter(addressesAdapter);
        addressesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String address = addresses.get(position);
                ArrayList<Place> chosePlace = new ArrayList<>();
                chosePlace.add(addressesMap.get(address));
                Intent intent = new Intent(DealAddressActivity.this, MapsActivity.class);

                intent.putParcelableArrayListExtra(DealInformationFragment.EXTRA_PLACE,
                        chosePlace);
                startActivity(intent);
            }
        });
    }
}
