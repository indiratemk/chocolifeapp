package com.example.indira.chocotestproject.deal_info;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.example.indira.chocotestproject.DealInfoActivity;
import com.example.indira.chocotestproject.R;
import com.example.indira.chocotestproject.model.DealInfo;
import com.example.indira.chocotestproject.util.Constant;

/**
 * A simple {@link Fragment} subclass.
 */
public class DealFeaturesFragment extends Fragment {
    private static final String TAG = "DealFeaturesFragment";
    private static final String ARG_DEAL_INFO = "deal_info";


    public static DealFeaturesFragment getInstance(DealInfo dealInfo) {
        DealFeaturesFragment fragment = new DealFeaturesFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_DEAL_INFO, dealInfo);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater
                .inflate(R.layout.fragment_deal_features, container, false);
        WebView featuresWebView = view.findViewById(R.id.deal_info_features);
        Bundle bundle = getArguments();
        String htmlAsString;
        if (bundle != null) {
            DealInfo dealInfo = bundle.getParcelable(DealInfoActivity.ARG_DEAL_INFO);
            htmlAsString = dealInfo.getFeatures();
            String htmlStringBuilder = Constant.HTML_START +
                                        htmlAsString +
                                        Constant.HTML_END;
            featuresWebView.loadDataWithBaseURL(Constant.CSS_PATH, htmlStringBuilder,
                    "text/html", "utf-8", null);
        }
        return view;
    }

}
