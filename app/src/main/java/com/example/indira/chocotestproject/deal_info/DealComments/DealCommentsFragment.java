package com.example.indira.chocotestproject.deal_info.DealComments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.indira.chocotestproject.R;
import com.example.indira.chocotestproject.deal.DealAdapter;
import com.example.indira.chocotestproject.model.DealComment;
import com.example.indira.chocotestproject.util.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DealCommentsFragment extends Fragment implements IDealCommentsView {
    private static final String TAG = "DealCommentsFragment";
    public static final String ARG_DEAL_ID = "deal_id";
    public static final String ARG_QUESTION_NUMBER = "question_number";

    private RecyclerView commentsRecyclerView;
    private DealCommentsAdapter commentsAdapter;

    private List<DealComment> dealComments = new ArrayList<>();

    private int dealId;
    private int pageAmount;
    private int questionsAmount;
    private int current_page = 1;

    private IDealCommentsPresenter dealCommentsPresenter;


    public static DealCommentsFragment getInstance(int dealId, int questionNumber) {
        DealCommentsFragment fragment = new DealCommentsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_DEAL_ID, dealId);
        bundle.putInt(ARG_QUESTION_NUMBER, questionNumber);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_deal_comments, container, false);

        Bundle bundle = getArguments();
        if (bundle != null) {
            dealId = bundle.getInt(ARG_DEAL_ID, -1);
            questionsAmount = bundle.getInt(ARG_QUESTION_NUMBER, -1);
            dealCommentsPresenter = new DealCommentsPresenterImpl(this);
            dealCommentsPresenter.getComments(dealId, current_page);
        }
        updatePageAmount();

        commentsRecyclerView = view.findViewById(R.id.deal_comments_recycler_view);
        return view;

    }

    @Override
    public void displayComments(List<DealComment> dealComments) {

        if (this.dealComments.isEmpty()) {
            this.dealComments = dealComments;
            commentsAdapter = new DealCommentsAdapter(getActivity(), this.dealComments);
            commentsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            commentsRecyclerView.setAdapter(commentsAdapter);
        } else {
            if (dealComments.size() == questionsAmount
                    || pageAmount < current_page) {
                current_page = Constant.INITIAL_PAGE;
                return;
            }
            this.dealComments.addAll(dealComments);
            commentsAdapter.notifyDataSetChanged();
        }
        commentsAdapter.setOnEndReachedListener(new DealAdapter.OnEndReachedListener() {
            @Override
            public void onEndReached() {
                current_page += 1;
                dealCommentsPresenter.getComments(dealId, current_page);
            }
        });
    }

    @Override
    public void displayError() {
        Toast.makeText(getActivity(),
                "Пожалуйста проверьте Интернет соединение", Toast.LENGTH_SHORT).show();
    }


    public void updatePageAmount() {
        pageAmount = questionsAmount/ Constant.AMOUNT_ITEMS_IN_PAGE;
        if (questionsAmount/(double) Constant.AMOUNT_ITEMS_IN_PAGE > pageAmount) {
            pageAmount += 1;
        }
    }
}
