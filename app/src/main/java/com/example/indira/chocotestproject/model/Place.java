package com.example.indira.chocotestproject.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.Html;
import android.text.Spanned;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Place implements Parcelable {

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("lat")
    @Expose
    private double lat;

    @SerializedName("lon")
    @Expose
    private double lon;

    @SerializedName("schedule")
    @Expose
    private String schedule;

    @SerializedName("phones")
    @Expose
    private List<String> phones;


    protected Place(Parcel in) {
        address = in.readString();
        lat = in.readDouble();
        lon = in.readDouble();
        schedule = in.readString();
        phones = in.createStringArrayList();
    }

    public static final Creator<Place> CREATOR = new Creator<Place>() {
        @Override
        public Place createFromParcel(Parcel in) {
            return new Place(in);
        }

        @Override
        public Place[] newArray(int size) {
            return new Place[size];
        }
    };

    public String getAddress() {
        return address;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public String getSchedule() {
        return schedule;
    }

    public List<String> getPhones() {
        return phones;
    }

    public String getPhonesAsString() {
        String phonesString = "";
        for (String phone : phones) {
            phonesString += phone + "\n";
        }
        return phonesString.trim();
    }

    public Spanned getScheduleAsHtml() {
        Spanned scheduleSpanned = Html.fromHtml(schedule);
        return scheduleSpanned;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(address);
        dest.writeDouble(lat);
        dest.writeDouble(lon);
        dest.writeString(schedule);
        dest.writeStringList(phones);
    }
}
