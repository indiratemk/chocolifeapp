package com.example.indira.chocotestproject.deal;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.indira.chocotestproject.R;

public class SortingDialog extends AppCompatDialogFragment {
    private static final String TAG = "SortingDialog";
    private static final String ARG_CHECKED_ITEM_ID = "checked_item_id";

    private RadioGroup sortingGroup;
    private OnSortingDialogListener listener;

    public static SortingDialog newInstance(int checkedItemId) {
        Bundle args = new Bundle();
        args.putInt(ARG_CHECKED_ITEM_ID, checkedItemId);
        SortingDialog fragment = new SortingDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.layout_choose_sorting_dialog, null);
        sortingGroup = view.findViewById(R.id.deal_sorting_radio_group);

        if (getArguments() != null) {
            int checkedItemId = getArguments().getInt(ARG_CHECKED_ITEM_ID);
            sortingGroup.check(checkedItemId);
        }

        sortingGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                String sortingParameter = null;
                switch (checkedId) {
                    case R.id.radio_by_default:
                        break;
                    case R.id.radio_by_popularity:
                        sortingParameter = "popular";
                        break;
                    case R.id.radio_by_rating:
                        sortingParameter = "rating_desc";
                        break;
                    case R.id.radio_by_new_first:
                        sortingParameter = "new";
                        break;
                    case R.id.radio_by_price_increasing:
                        sortingParameter = "price_asc";
                        break;
                    case R.id.radio_by_price_decreasing:
                        sortingParameter = "price_desc";
                        break;
                }
                listener.sortingChanged(sortingParameter, checkedId);
            }
        });

        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnSortingDialogListener) context;
        } catch (ClassCastException e) {
            Log.e(TAG, "onAttach: ClassCastException " + e.getMessage());
        }
    }

    public interface OnSortingDialogListener {
        void sortingChanged(String sortingParameter, int checkedItemId);
    }


}
