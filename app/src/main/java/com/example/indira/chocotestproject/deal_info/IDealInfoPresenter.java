package com.example.indira.chocotestproject.deal_info;

public interface IDealInfoPresenter {
    void getDealInfo(int dealId);
}
