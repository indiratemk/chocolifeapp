package com.example.indira.chocotestproject.deal_info.DealComments;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.indira.chocotestproject.R;
import com.example.indira.chocotestproject.deal.DealAdapter;
import com.example.indira.chocotestproject.model.DealComment;
import com.example.indira.chocotestproject.model.User;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class DealCommentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "DealCommentsAdapter";
    private static final int COMMENT_BUTTON_VIEW = 0;
    private static final int COMMENTS_VIEW = 1;

    private List<DealComment> dealComments;
    private Context context;

    DealAdapter.OnEndReachedListener onEndReachedListener;

    public DealCommentsAdapter(Context context, List<DealComment> dealComments) {
        this.dealComments = dealComments;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return COMMENT_BUTTON_VIEW;
        }
        return COMMENTS_VIEW;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view;
        switch (viewType) {
            case COMMENT_BUTTON_VIEW:
                view = LayoutInflater.from(context)
                        .inflate(R.layout.comment_button_item, viewGroup, false);
                return new DealCommentButtonHolder(view);
            default:
                view = LayoutInflater.from(context)
                        .inflate(R.layout.comment_item, viewGroup, false);
                return new DealCommentsHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        Log.d(TAG, "onBindViewHolder: current comment: " + position);
        if (viewHolder instanceof DealCommentsHolder) {
            if (dealComments != null && dealComments.size() > 0) {
                int dealPosition = position - 1;
                DealComment dealComment = dealComments.get(dealPosition);
                ((DealCommentsHolder) viewHolder).bindTo(dealComment);
                if (position == dealComments.size() - 2) {
                    onEndReachedListener.onEndReached();
                }
            }
        }
    }


    @Override
    public int getItemCount() {
        if (dealComments != null) {
            return dealComments.size() + 1;
        }
        return 0;
    }

    class DealCommentsHolder extends RecyclerView.ViewHolder {
        private CircleImageView userAvatar;
        private TextView userName;
        private TextView userComment;
        private CircleImageView employeeAvatar;
        private TextView employeeName;
        private TextView employeeAnswer;
        private TextView commentTime;

        public DealCommentsHolder(@NonNull View itemView) {
            super(itemView);

            userAvatar = itemView.findViewById(R.id.user_avatar);
            userName = itemView.findViewById(R.id.user_name);
            userComment = itemView.findViewById(R.id.user_comment);
            employeeAvatar = itemView.findViewById(R.id.employee_avatar);
            employeeName = itemView.findViewById(R.id.employee_name);
            employeeAnswer = itemView.findViewById(R.id.employee_answer);
            commentTime = itemView.findViewById(R.id.comment_date);
        }

        public void bindTo(DealComment dealComment) {
            User user = dealComment.getUser();
            User employee = dealComment.getAnswer().getUser();

            Picasso.get().load(user.getAvatarUrl()).into(userAvatar);
            if (user.getName() == null || user.getName().isEmpty()) {
                userName.setText(context.getString(R.string.no_name_text));
            } else {
                userName.setText(user.getName());
            }
            userComment.setText(dealComment.getCommentText());

            Picasso.get().load(employee.getAvatarUrl()).into(employeeAvatar);
            employeeName.setText(employee.getName());
            employeeAnswer.setText(dealComment.getAnswer().getAnswerText());

            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date commentDate = format.parse(dealComment.getCommentTime());
                SimpleDateFormat updatedFormat = new SimpleDateFormat("dd.MM.yy");
                commentTime.setText(updatedFormat.format(commentDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    class DealCommentButtonHolder extends RecyclerView.ViewHolder {
        private Button commentButton;

        public DealCommentButtonHolder(@NonNull View itemView) {
            super(itemView);
            commentButton = itemView.findViewById(R.id.ask_question_button);
        }
    }

    public void setOnEndReachedListener(DealAdapter.OnEndReachedListener onEndReachedListener) {
        this.onEndReachedListener = onEndReachedListener;
    }
}
