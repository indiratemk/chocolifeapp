package com.example.indira.chocotestproject.deal;

import android.util.Log;

import com.example.indira.chocotestproject.MainActivity;
import com.example.indira.chocotestproject.model.Deal;
import com.example.indira.chocotestproject.model.JsonResponse;
import com.example.indira.chocotestproject.service.ServiceWrapper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DealPresenterImpl implements IDealPresenter {
    private static final String TAG = "DealPresenterImpl";

    private IDealView view;
    private ServiceWrapper serviceInstance;

    public DealPresenterImpl(MainActivity view) {
        this.view = view;
    }

    @Override
    public void getDeals(int cityId, int categoryId, String sortingParameter, int page) {
        serviceInstance = new ServiceWrapper();

        Call<JsonResponse<List<Deal>>> responseCall =
                serviceInstance.getDealsResponse(cityId, categoryId, sortingParameter, page);

        view.showLoading();

        responseCall.enqueue(new Callback<JsonResponse<List<Deal>>>() {
            @Override
            public void onResponse(Call<JsonResponse<List<Deal>>> call, Response<JsonResponse<List<Deal>>> response) {
                if (response.body() != null && response.isSuccessful()) {
                    view.displayDeals(response.body().getResponse());
                }
            }

            @Override
            public void onFailure(Call<JsonResponse<List<Deal>>> call, Throwable t) {
                view.displayError();
                view.hideLoading();
            }
        });
    }

    @Override
    public void getSearchDeals(int cityId, String searchParameter, int page) {
        serviceInstance = new ServiceWrapper();
        
        Call<JsonResponse<List<Deal>>> responseCall =
                serviceInstance.getSearchDealsResponse(cityId, searchParameter, page);
        
        responseCall.enqueue(new Callback<JsonResponse<List<Deal>>>() {
            @Override
            public void onResponse(Call<JsonResponse<List<Deal>>> call, Response<JsonResponse<List<Deal>>> response) {
                if (response.body() != null && response.isSuccessful()) {
                    view.displaySearchDeals(response.body().getResponse());
                }
                Log.d(TAG, "onResponse: search: " + response.body().getResponse());
            }

            @Override
            public void onFailure(Call<JsonResponse<List<Deal>>> call, Throwable t) {
                view.displayError();
            }
        });
    }
}
