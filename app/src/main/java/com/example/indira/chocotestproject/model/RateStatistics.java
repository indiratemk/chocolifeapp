package com.example.indira.chocotestproject.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RateStatistics implements Parcelable {

    @SerializedName("reviews_number")
    @Expose
    private int reviewsNumber;

    @SerializedName("four_stars_count")
    @Expose
    private int fourStarsCount;

    @SerializedName("three_stars_count")
    @Expose
    private int threeStarsCount;

    @SerializedName("one_star_count")
    @Expose
    private int oneStarCount;

    @SerializedName("five_stars_count")
    @Expose
    private int fiveStarsCount;

    @SerializedName("reviews_rate")
    @Expose
    private double reviewsRate;

    @SerializedName("two_stars_count")
    @Expose
    private int twoStarsCount;


    protected RateStatistics(Parcel in) {
        reviewsNumber = in.readInt();
        fourStarsCount = in.readInt();
        threeStarsCount = in.readInt();
        oneStarCount = in.readInt();
        fiveStarsCount = in.readInt();
        reviewsRate = in.readDouble();
        twoStarsCount = in.readInt();
    }

    public static final Creator<RateStatistics> CREATOR = new Creator<RateStatistics>() {
        @Override
        public RateStatistics createFromParcel(Parcel in) {
            return new RateStatistics(in);
        }

        @Override
        public RateStatistics[] newArray(int size) {
            return new RateStatistics[size];
        }
    };

    public int getReviewsNumber() {
        return reviewsNumber;
    }

    public int getFourStarsCount() {
        return fourStarsCount;
    }

    public int getThreeStarsCount() {
        return threeStarsCount;
    }

    public int getOneStarCount() {
        return oneStarCount;
    }

    public int getFiveStarsCount() {
        return fiveStarsCount;
    }

    public double getReviewsRate() {
        return reviewsRate;
    }

    public int getTwoStarsCount() {
        return twoStarsCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(reviewsNumber);
        dest.writeInt(fourStarsCount);
        dest.writeInt(threeStarsCount);
        dest.writeInt(oneStarCount);
        dest.writeInt(fiveStarsCount);
        dest.writeDouble(reviewsRate);
        dest.writeInt(twoStarsCount);
    }
}
