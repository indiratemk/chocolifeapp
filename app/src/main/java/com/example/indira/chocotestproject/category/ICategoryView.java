package com.example.indira.chocotestproject.category;

import com.example.indira.chocotestproject.model.Category;

import java.util.List;

public interface ICategoryView {
    void displayCategoryMenu(List<Category> categories);
}
