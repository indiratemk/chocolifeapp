package com.example.indira.chocotestproject.deal_info.DealReviews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.indira.chocotestproject.R;
import com.example.indira.chocotestproject.deal.DealAdapter;
import com.example.indira.chocotestproject.model.DealInfo;
import com.example.indira.chocotestproject.model.DealReview;

import java.util.List;

public class DealReviewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "DealReviewsAdapter";

    private static final int REVIEW_STATISTICS_VIEW = 0;
    private static final int REVIEWS_VIEW= 1;

    private List<DealReview> dealReviews;
    private DealInfo dealInfo;
    private Context context;
    private OnSortBySelectListener onSortBySelectListener;
    private DealAdapter.OnEndReachedListener onEndReachedListener;

    public DealReviewsAdapter(Context context, List<DealReview> dealReviews, DealInfo dealInfo) {
        this.context = context;
        this.dealReviews = dealReviews;
        this.dealInfo = dealInfo;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return REVIEW_STATISTICS_VIEW;
        }
        return REVIEWS_VIEW;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view;
        switch (viewType) {
            case REVIEW_STATISTICS_VIEW:
                view = LayoutInflater.from(context)
                        .inflate(R.layout.review_statistics_item, viewGroup, false);
                return new DealReviewStatisticsHolder(view);
            default:
                view = LayoutInflater.from(context)
                        .inflate(R.layout.review_item, viewGroup, false);
                return new DealReviewsHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        Log.d(TAG, "onBindViewHolder: position: " + position);
        if (viewHolder instanceof DealReviewsHolder) {
            if (dealReviews != null && dealReviews.size() > 0) {
                int dealPosition = position - 1;
                DealReview dealReview = dealReviews.get(dealPosition);
                ((DealReviewsHolder) viewHolder).bindTo(context, dealReview);
                if (position == dealReviews.size() - 2) {
                    onEndReachedListener.onEndReached();
                }
            }
        } else if (viewHolder instanceof DealReviewStatisticsHolder) {
            if (dealInfo != null) {
                Log.d(TAG, "onBindViewHolder: bindTo " + 1);
                ((DealReviewStatisticsHolder) viewHolder).bindTo(context, dealInfo, onSortBySelectListener);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (dealReviews != null) {
            return dealReviews.size() + 1;
        }
        return 0;
    }

    interface OnSortBySelectListener {
        void onSortBySelect(int userMark);
    }

    public void setOnSortBySelectListener(OnSortBySelectListener onSortBySelectListener) {
        this.onSortBySelectListener = onSortBySelectListener;
    }

    public void setOnEndReachedListener(DealAdapter.OnEndReachedListener onEndReachedListener) {
        this.onEndReachedListener = onEndReachedListener;
    }
}
