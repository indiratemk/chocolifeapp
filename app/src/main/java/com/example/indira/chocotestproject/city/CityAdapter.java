package com.example.indira.chocotestproject.city;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.indira.chocotestproject.R;
import com.example.indira.chocotestproject.model.City;

import java.util.List;


public class CityAdapter extends RecyclerView.Adapter<CityAdapter.CityHolder> {
    private static final String TAG = "CityAdapter";

    private List<City> cities;
    private int selectedItemIndex;
    private CitiesDialog citiesDialog;


    public CityAdapter(CitiesDialog citiesDialog, List<City> cities) {
        this.citiesDialog = citiesDialog;
        this.cities = cities;
    }


    @NonNull
    @Override
    public CityHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.city_item, viewGroup, false);
        return new CityHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CityHolder cityHolder, final int i) {
        final City city = cities.get(i);
        cityHolder.city.setText(city.getTitle());
        cityHolder.city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedItemIndex = i;
                citiesDialog.selectCity(i, city.getTitle(), city.getId());
                citiesDialog.dismiss();
                notifyDataSetChanged();
            }
        });
        if (selectedItemIndex == i) {
            cityHolder.city.setBackgroundResource(R.color.colorHighlightItem);
            cityHolder.city.setTypeface(Typeface.DEFAULT_BOLD);

        } else {
            cityHolder.city.setBackgroundResource(R.color.colorWhite);
            cityHolder.city.setTypeface(Typeface.DEFAULT);
        }
    }

    @Override
    public int getItemCount() {
        if (cities == null) {
            return 0;
        }
        return cities.size();
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    class CityHolder extends RecyclerView.ViewHolder {
        private TextView city;

        public CityHolder(@NonNull View itemView) {
            super(itemView);
            city = itemView.findViewById(R.id.city_item_text_view);
        }
    }

    public void setSelectedItemIndex(int selectedItemIndex) {
        this.selectedItemIndex = selectedItemIndex;
    }
}
