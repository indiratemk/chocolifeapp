package com.example.indira.chocotestproject.city;

import android.util.Log;

import com.example.indira.chocotestproject.MainActivity;
import com.example.indira.chocotestproject.model.City;
import com.example.indira.chocotestproject.model.JsonResponse;
import com.example.indira.chocotestproject.service.ServiceWrapper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CityPresenterImpl implements ICityPresenter {
    private static final String TAG = "CityPresenterImpl";

    private ICityView view;
    private ServiceWrapper serviceInstance;

    public CityPresenterImpl(ICityView view) {
        this.view = view;
    }

    @Override
    public void getCities() {
        serviceInstance = new ServiceWrapper();

        Call<JsonResponse<List<City>>> responseCall = serviceInstance.getCitiesResponse();
        responseCall.enqueue(new Callback<JsonResponse<List<City>>>() {
            @Override
            public void onResponse(Call<JsonResponse<List<City>>> call, Response<JsonResponse<List<City>>> response) {
                if (response.body() != null && response.isSuccessful()) {
                    view.displayCities(response.body().getResponse());
                }
            }

            @Override
            public void onFailure(Call<JsonResponse<List<City>>> call, Throwable t) {
                Log.e(TAG, "onFailure: city request: " + t.getMessage() );
            }
        });
    }
}
