package com.example.indira.chocotestproject.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Partner implements Parcelable {

    @SerializedName("rate")
    @Expose
    private double partnerRate;

    protected Partner(Parcel in) {
        partnerRate = in.readDouble();
    }

    public static final Creator<Partner> CREATOR = new Creator<Partner>() {
        @Override
        public Partner createFromParcel(Parcel in) {
            return new Partner(in);
        }

        @Override
        public Partner[] newArray(int size) {
            return new Partner[size];
        }
    };

    public double getPartnerRate() {
        return partnerRate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(partnerRate);
    }
}
