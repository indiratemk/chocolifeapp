package com.example.indira.chocotestproject.local_data;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.indira.chocotestproject.model.Deal;

import java.util.List;

public class DealViewModel extends AndroidViewModel {
    private DealRepository dealRepository;
    private LiveData<List<Deal>> allDeals;

    public DealViewModel(@NonNull Application application) {
        super(application);
        dealRepository = new DealRepository(application);
        allDeals = dealRepository.getAllDeals();
    }

    public void insertDeal(Deal deal) {
        dealRepository.insertDeal(deal);
    }

    public void deleteDeal(Deal deal) {
        dealRepository.deleteDeal(deal);
    }

    public void deleteAllDeals() {
        dealRepository.deleteAllDeals();
    }

    public LiveData<List<Deal>> getAllDeals() {
        return allDeals;
    }
}
