package com.example.indira.chocotestproject.service;


import com.example.indira.chocotestproject.model.Category;
import com.example.indira.chocotestproject.model.City;
import com.example.indira.chocotestproject.model.Deal;
import com.example.indira.chocotestproject.model.DealComment;
import com.example.indira.chocotestproject.model.DealInfo;
import com.example.indira.chocotestproject.model.DealReview;
import com.example.indira.chocotestproject.model.JsonResponse;
import com.example.indira.chocotestproject.util.Constant;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceWrapper {
    private static Retrofit retrofit;
    private ApiService service;

    public ServiceWrapper() {
        getRetrofit();
        service = retrofit.create(ApiService.class);
    }

    private static void getRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
    }

    public Call<JsonResponse<List<City>>> getCitiesResponse() {
        return service.getCitiesResponse();
    }

    public Call<JsonResponse<List<Category>>> getCategoriesResponse(int cityId) {
        return service.getCategoriesResponse(cityId);
    }

    public Call<JsonResponse<DealInfo>> getDealInfoResponse(int dealId) {
        return service.getDealInfoResponse(dealId);
    }

    public Call<JsonResponse<List<DealComment>>> getDealCommentResponse(int dealId, int page) {
        return service.getDealCommentResponse(dealId, page);
    }

    public Call<JsonResponse<List<DealReview>>> getDealReviewResponse(int dealId, int page, int userMark) {
        return service.getDealReviewResponse(dealId, page, userMark);
    }

    public Call<JsonResponse<List<Deal>>> getDealsResponse(int cityId, int categoryId,
                                                           String sortingParameter, int page) {
        return service.getDealsResponse(cityId, categoryId, sortingParameter, page);
    }

    public Call<JsonResponse<List<Deal>>> getSearchDealsResponse(int cityId, String searchParameter,
                                                                 int page) {
        return service.getSearchDealsResponse(cityId, searchParameter, page);
    }

}
