package com.example.indira.chocotestproject.city;

import com.example.indira.chocotestproject.model.City;

import java.util.List;

public interface ICityPresenter {
    void getCities();
}
