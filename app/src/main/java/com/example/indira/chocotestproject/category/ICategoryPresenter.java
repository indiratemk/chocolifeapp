package com.example.indira.chocotestproject.category;

public interface ICategoryPresenter {
    void getCategoryMenuItems(int cityId);
}
