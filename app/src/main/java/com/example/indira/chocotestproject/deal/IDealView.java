package com.example.indira.chocotestproject.deal;

import com.example.indira.chocotestproject.model.Deal;

import java.util.List;

public interface IDealView {
    void displayDeals(List<Deal> deals);
    void displaySearchDeals(List<Deal> deals);
    void displayError();

    void showLoading();
    void hideLoading();

//    void displaySearchError();
}
