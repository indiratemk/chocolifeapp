package com.example.indira.chocotestproject.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "deal_table")
public class Deal {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int orderingId;

    @SerializedName("deal_id")
    @Expose
    private int dealId;

    @SerializedName("deal_kind")
    @Expose
    private String dealKind;

    @SerializedName("title_short")
    @Expose
    private String dealShortTitle;

    @SerializedName("image_kind")
    @Expose
    private String dealImageKind;

    @SerializedName("reviews_rate")
    @Expose
    private double dealReviewsRate;

    @SerializedName("title")
    @Expose
    private String dealTitle;

    @SerializedName("bought")
    @Expose
    private int dealBoughtAmount;

    @SerializedName("price")
    @Expose
    private int dealPrice;

    @SerializedName("image_url")
    @Expose
    private String dealImageUrl;

    @SerializedName("open_action")
    @Expose
    private String dealOpenAction;

    @SerializedName("link")
    @Expose
    private String dealLink;


    public Deal(int dealId, String dealKind, String dealShortTitle, String dealImageKind,
                double dealReviewsRate, String dealTitle, int dealBoughtAmount, int dealPrice,
                String dealImageUrl, String dealOpenAction, String dealLink) {
        this.dealId = dealId;
        this.dealKind = dealKind;
        this.dealShortTitle = dealShortTitle;
        this.dealImageKind = dealImageKind;
        this.dealReviewsRate = dealReviewsRate;
        this.dealTitle = dealTitle;
        this.dealBoughtAmount = dealBoughtAmount;
        this.dealPrice = dealPrice;
        this.dealImageUrl = dealImageUrl;
        this.dealOpenAction = dealOpenAction;
        this.dealLink = dealLink;
    }

    public void setOrderingId(int orderingId) {
        this.orderingId = orderingId;
    }

    public int getDealId() {
        return dealId;
    }

    public String getDealKind() {
        return dealKind;
    }

    public String getDealShortTitle() {
        return dealShortTitle;
    }

    public String getDealImageKind() {
        return dealImageKind;
    }

    public double getDealReviewsRate() {
        return dealReviewsRate;
    }

    public String getDealTitle() {
        return dealTitle;
    }

    public int getDealBoughtAmount() {
        return dealBoughtAmount;
    }

    public int getDealPrice() {
        return dealPrice;
    }

    public String getDealImageUrl() {
        return dealImageUrl;
    }

    public String getDealOpenAction() {
        return dealOpenAction;
    }

    public String getDealLink() {
        return dealLink;
    }

    public int getOrderingId() {
        return orderingId;
    }
}
