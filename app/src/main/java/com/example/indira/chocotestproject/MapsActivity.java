package com.example.indira.chocotestproject;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.indira.chocotestproject.deal_info.DealInformationFragment;
import com.example.indira.chocotestproject.model.DealInfo;
import com.example.indira.chocotestproject.model.Place;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String TAG = "MapsActivity";
    private static final int REQUEST_LOCATION_PERMISSION = 1;

    private GoogleMap mMap;
    private LatLng dealPosition;

    private TextView dealAddress;
    private TextView dealContacts;
    private TextView dealSchedule;
    private CardView infoWindow;

    private float zoom = 15;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);


        dealAddress = findViewById(R.id.deal_info_location_text);
        dealContacts = findViewById(R.id.deal_info_contact_number);
        dealSchedule = findViewById(R.id.deal_info_schedule);
        infoWindow = findViewById(R.id.map_info_window);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.map_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.focus) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(dealPosition, zoom));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        Map<Marker, Place> markerInfoMap = new HashMap<>();
        Intent intent = getIntent();
        List<Place> places;
        places = intent.getParcelableArrayListExtra(DealInformationFragment.EXTRA_PLACE);
        dealPosition = new LatLng(places.get(0).getLat(), places.get(0).getLon());
        Log.d(TAG, "onMapReady: " + places.get(0).getAddress());
        enableMyLocation();


        for (Place place : places) {
            addNewMarkers(place, markerInfoMap);
        }
    }

    private void addNewMarkers(final Place place, final Map<Marker, Place> markerPlaceMap) {
        LatLng position = new LatLng(place.getLat(), place.getLon());
        Marker marker = mMap.addMarker(new MarkerOptions().position(position));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, zoom));

        markerPlaceMap.put(marker, place);

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                infoWindow.setVisibility(View.GONE);
            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                dealAddress.setText(markerPlaceMap.get(marker).getAddress());
                dealContacts.setText(markerPlaceMap.get(marker).getPhonesAsString());
                dealSchedule.setText(markerPlaceMap.get(marker).getScheduleAsHtml());
                infoWindow.setVisibility(View.VISIBLE);
                return false;
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION:
                if (grantResults.length > 0
                        && grantResults[0]
                        == PackageManager.PERMISSION_GRANTED) {
                    enableMyLocation();
                    break;
                }
        }
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            ActivityCompat.requestPermissions(this, new String[]
                            {Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION_PERMISSION);
        }
    }
}
