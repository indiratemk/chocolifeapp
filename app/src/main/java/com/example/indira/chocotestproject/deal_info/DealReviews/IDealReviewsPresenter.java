package com.example.indira.chocotestproject.deal_info.DealReviews;

public interface IDealReviewsPresenter {
    void getReviews(int dealId, int page, int userMark);
}
