package com.example.indira.chocotestproject.city;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.example.indira.chocotestproject.R;
import com.example.indira.chocotestproject.model.City;

import java.util.ArrayList;
import java.util.List;

public class CitiesDialog extends AppCompatDialogFragment {
    private static final String TAG = "CitiesDialog";
    private static final String ARG_CITY_LIST = "cities";
    private static final String ARG_SELECTED_CITY_INDEX = "selected_city_index";

    private RecyclerView citiesRecyclerView;
    private CityAdapter cityAdapter;
    private OnCityDialogListener listener;
    private static List<City> cities = new ArrayList<>();

    private static int citySelectedIndex;

//    TODO make better this method of selecting city

    public static CitiesDialog newInstance(ArrayList<City> citiesList, int selectedCityIndex) {
//        Bundle args = new Bundle();
//        args.putParcelableArrayList(ARG_CITY_LIST, cities);
//        args.putInt(ARG_SELECTED_CITY_INDEX, selectedCityIndex);
        cities = citiesList;
        citySelectedIndex = selectedCityIndex;
        CitiesDialog fragment = new CitiesDialog();
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.layout_choose_city_dialog, null);

        citiesRecyclerView = view.findViewById(R.id.cities_recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        citiesRecyclerView.setLayoutManager(layoutManager);



        Log.d(TAG, "onCreateDialog: index: " + citySelectedIndex);
//        if (getArguments() != null) {
//            cities = getArguments().getParcelableArrayList(ARG_CITY_LIST);
//            citySelectedIndex = getArguments().getInt(ARG_SELECTED_CITY_INDEX);
//        }
        cityAdapter = new CityAdapter(CitiesDialog.this, cities);
        cityAdapter.setCities(cities);
        cityAdapter.setSelectedItemIndex(citySelectedIndex);
        citiesRecyclerView.setAdapter(cityAdapter);

        builder.setView(view);

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnCityDialogListener) context;
        } catch (ClassCastException e) {
            Log.e(TAG, "onAttach: ClassCastException " + e.getMessage());
        }
    }

    public interface OnCityDialogListener {
        void cityChanged(String cityTitle, int cityId, int index);
    }

    public void selectCity(int index, String cityTitle, int cityId) {
        citySelectedIndex = index;
        listener.cityChanged(cityTitle, cityId, index);
    }

}
