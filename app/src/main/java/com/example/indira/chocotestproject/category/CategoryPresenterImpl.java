package com.example.indira.chocotestproject.category;

import android.util.Log;

import com.example.indira.chocotestproject.MainActivity;
import com.example.indira.chocotestproject.model.Category;
import com.example.indira.chocotestproject.model.JsonResponse;
import com.example.indira.chocotestproject.service.ServiceWrapper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryPresenterImpl implements ICategoryPresenter {
    private static final String TAG = "CategoryPresenterImpl";

    private ICategoryView view;
    private ServiceWrapper serviceInstance;

    public CategoryPresenterImpl(MainActivity view) {
        this.view = view;
    }

    @Override
    public void getCategoryMenuItems(int cityId) {
        serviceInstance = new ServiceWrapper();

        Call<JsonResponse<List<Category>>> responseCall = serviceInstance.getCategoriesResponse(cityId);
        responseCall.enqueue(new Callback<JsonResponse<List<Category>>>() {
            @Override
            public void onResponse(Call<JsonResponse<List<Category>>> call, Response<JsonResponse<List<Category>>> response) {
                if (response.body() != null && response.isSuccessful()) {
                    view.displayCategoryMenu(response.body().getResponse());
                }
            }

            @Override
            public void onFailure(Call<JsonResponse<List<Category>>> call, Throwable t) {
                Log.e(TAG, "onFailure: category request: " + t.getMessage() );
            }
        });
    }
}
