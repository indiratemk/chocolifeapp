package com.example.indira.chocotestproject.local_data;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.indira.chocotestproject.model.Deal;

import java.util.List;

public class DealRepository {
    private DealDao dealDao;
    private LiveData<List<Deal>> allDeals;

    public DealRepository(Application application) {
        DealDatabase database = DealDatabase.getInstance(application);
        dealDao = database.dealDao();
        allDeals = dealDao.getAllDeals();
    }

    public void insertDeal(Deal deal) {
        new InsertDealAsyncTask(dealDao).execute(deal);
    }

    public void deleteDeal(Deal deal) {
        new DeleteDealAsyncTask(dealDao).execute(deal);
    }

    public void deleteAllDeals() {
        new DeleteAllDealsAsyncTask(dealDao).execute();
    }

    public LiveData<List<Deal>> getAllDeals() {
        return allDeals;
    }

    private static class InsertDealAsyncTask extends AsyncTask<Deal, Void, Void> {
        private DealDao dealDao;

        public InsertDealAsyncTask(DealDao dealDao) {
            this.dealDao = dealDao;
        }

        @Override
        protected Void doInBackground(Deal... deals) {
            dealDao.insertDeal(deals[0]);
            return null;
        }
    }

    private static class DeleteDealAsyncTask extends AsyncTask<Deal, Void, Void> {
        private DealDao dealDao;

        public DeleteDealAsyncTask(DealDao dealDao) {
            this.dealDao = dealDao;
        }

        @Override
        protected Void doInBackground(Deal... deals) {
            dealDao.deleteDeal(deals[0]);
            return null;
        }
    }

    private static class DeleteAllDealsAsyncTask extends AsyncTask<Void, Void, Void> {
        private DealDao dealDao;

        public DeleteAllDealsAsyncTask(DealDao dealDao) {
            this.dealDao = dealDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            dealDao.deleteAllDeals();
            return null;
        }
    }
}
