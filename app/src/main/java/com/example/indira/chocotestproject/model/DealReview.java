package com.example.indira.chocotestproject.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DealReview {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("user_mark")
    @Expose
    private int mark;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("recommends")
    @Expose
    private boolean isRecommend;

    @SerializedName("user_simple_comment")
    @Expose
    private String userSimpleComment;

    @SerializedName("user")
    @Expose
    private User user;

    public int getId() {
        return id;
    }

    public int getMark() {
        return mark;
    }

    public String getDate() {
        return date;
    }

    public boolean isRecommend() {
        return isRecommend;
    }

    public String getUserSimpleComment() {
        return userSimpleComment;
    }

    public User getUser() {
        return user;
    }
}
