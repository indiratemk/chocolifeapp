package com.example.indira.chocotestproject.category;

public interface OnCategoryClickListener {
    void onCategoryClick(int categoryId, int dealsNumber);
}
