package com.example.indira.chocotestproject.deal;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.indira.chocotestproject.R;
import com.example.indira.chocotestproject.model.Deal;
import com.squareup.picasso.Picasso;

class GeneralDealHolder extends RecyclerView.ViewHolder {
    private ImageView dealImage;
    private TextView dealShortTitle;
    private TextView dealReviewsRate;
    private TextView dealTitle;
    private TextView dealBoughtAmount;
    private TextView dealPrice;
    private ImageView dealFavoriteIcon;


    public GeneralDealHolder(@NonNull View itemView) {
        super(itemView);
        dealImage = itemView.findViewById(R.id.general_image_view);
        dealShortTitle = itemView.findViewById(R.id.deal_short_title);
        dealReviewsRate = itemView.findViewById(R.id.deal_reviews_rate);
        dealTitle = itemView.findViewById(R.id.deal_title);
        dealBoughtAmount = itemView.findViewById(R.id.deal_bought);
        dealPrice = itemView.findViewById(R.id.deal_price);
        dealFavoriteIcon = itemView.findViewById(R.id.ic_favorite_image_view);
    }

    public void bindTo(Deal deal) {
        Picasso.get().load(deal.getDealImageUrl()).into(dealImage);
        dealShortTitle.setText(deal.getDealShortTitle());
        dealPrice.setText(String.valueOf(deal.getDealPrice()));
        dealBoughtAmount
                .setText(String.valueOf(deal.getDealBoughtAmount()));
        dealTitle.setText(deal.getDealTitle());
        if (deal.getDealReviewsRate() == 0) {
            dealReviewsRate.setVisibility(View.GONE);
            dealFavoriteIcon.setVisibility(View.GONE);
        } else {
            dealReviewsRate
                    .setText(String.valueOf(deal.getDealReviewsRate()));
        }
    }
}
