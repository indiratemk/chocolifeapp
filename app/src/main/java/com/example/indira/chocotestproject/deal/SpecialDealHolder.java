package com.example.indira.chocotestproject.deal;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.example.indira.chocotestproject.R;
import com.example.indira.chocotestproject.model.Deal;
import com.squareup.picasso.Picasso;

class SpecialDealHolder extends RecyclerView.ViewHolder {
    private ImageView dealImage;

    public SpecialDealHolder(@NonNull View itemView) {
        super(itemView);
        dealImage = itemView.findViewById(R.id.wide_plate_image_view);
    }

    public void bindTo(Deal deal) {
        Picasso.get().load(deal.getDealImageUrl()).into(dealImage);
    }
}
