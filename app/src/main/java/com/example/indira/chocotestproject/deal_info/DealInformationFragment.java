package com.example.indira.chocotestproject.deal_info;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.example.indira.chocotestproject.DealAddressActivity;
import com.example.indira.chocotestproject.DealInfoActivity;
import com.example.indira.chocotestproject.MapsActivity;
import com.example.indira.chocotestproject.R;
import com.example.indira.chocotestproject.model.DealInfo;
import com.example.indira.chocotestproject.model.Place;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DealInformationFragment extends Fragment implements OnMapReadyCallback {
    private static final String TAG = "DealInformationFragment";

    private static final String ARG_DEAL_INFO = "deal_info";
    public static final String EXTRA_PLACE = "com.example.indira.chocotestproject.extra.PLACE";

    private ViewFlipper imageViewFlipper;
    private TextView shortTitleTextView;
    private TextView titleTextView;
    private TextView ratingTextView;
    private TextView priceTextView;
    private TextView economyTextView;
    private TextView boughtTextView;
    private TextView reviewsNumberTextView;
    private TextView dealTimeoutTextView;
    private FrameLayout mapLayout;

    private TextView addressTextView;
    private TextView contactsTextView;
    private TextView scheduleTextView;

    private TextView contactsTitleTextView;
    private CardView locationCardView;

    private TextView moreAddressLink;
    private View line;

    private GoogleMap googleMap;

    private float zoom = 15;
    private double lat;
    private double lon;
    private List<Place> places;


    public static DealInformationFragment getInstance(DealInfo dealInfo) {
        DealInformationFragment fragment = new DealInformationFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_DEAL_INFO, dealInfo);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        View view = inflater.inflate(R.layout.fragment_deal_information, container, false);

        init(view);

        if (bundle != null) {
            DealInfo dealInfo = bundle.getParcelable(DealInfoActivity.ARG_DEAL_INFO);
            if (dealInfo != null) {
                bindTo(dealInfo);
            }
        }
        return view;
    }

    private void init(View view) {
        imageViewFlipper = view.findViewById(R.id.deal_info_view_flipper);
        shortTitleTextView = view.findViewById(R.id.deal_info_short_title);
        titleTextView = view.findViewById(R.id.deal_info_long_title);
        ratingTextView = view.findViewById(R.id.deal_info_rating);
        priceTextView = view.findViewById(R.id.deal_info_price);
        economyTextView = view.findViewById(R.id.deal_info_economy);
        boughtTextView = view.findViewById(R.id.deal_info_bought);
        reviewsNumberTextView = view.findViewById(R.id.deal_info_reviews_number);
        dealTimeoutTextView = view.findViewById(R.id.deal_info_end_date);
        mapLayout = view.findViewById(R.id.resized_map);

        addressTextView = view.findViewById(R.id.deal_info_location_text);
        contactsTextView = view.findViewById(R.id.deal_info_contact_number);
        scheduleTextView = view.findViewById(R.id.deal_info_schedule);

        contactsTitleTextView = view.findViewById(R.id.deal_info_contacts);
        locationCardView = view.findViewById(R.id.deal_info_location_description);

        moreAddressLink = view.findViewById(R.id.link_more_address);
        line = view.findViewById(R.id.horizontal_line4);
    }

    private void flipperImage(String imageUrl) {
        ImageView sliderImageView = new ImageView(getActivity());
        sliderImageView.setAdjustViewBounds(true);
        sliderImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        Picasso.get().load(imageUrl).into(sliderImageView);

        imageViewFlipper.addView(sliderImageView);
        imageViewFlipper.setFlipInterval(4000);
        imageViewFlipper.setAutoStart(true);

        imageViewFlipper.setInAnimation(getActivity(), R.anim.slide_in_right);
        imageViewFlipper.setOutAnimation(getActivity(), R.anim.slide_out_left);
    }

    private void bindTo(DealInfo dealInfo) {
        List<String> imageUrls = dealInfo.getImageUrls();
        for (String imageUrl : imageUrls) {
            flipperImage(imageUrl);
        }

        shortTitleTextView.setText(dealInfo.getShortTitle());
        titleTextView.setText(dealInfo.getTitle());
        ratingTextView.setText(String.valueOf(dealInfo.getReviewsRate()));
        priceTextView.setText(getString(R.string.deal_info_price_text, dealInfo.getPrice()));
        economyTextView.setText(getString(R.string.deal_info_economy_text, dealInfo.getEconomy()));
        boughtTextView.setText(getString(R.string.deal_info_bought_text, dealInfo.getBought()));
        reviewsNumberTextView.setText(getString(R.string.deal_info_reviews_number_text,
                dealInfo.getReviewsNumber()));

        formatDate(dealInfo.getDealTimeout());


        places = dealInfo.getPlaces();
        if (!places.isEmpty()) {
            if (places.size() > 1) {
                moreAddressLink.setVisibility(View.VISIBLE);
                line.setVisibility(View.VISIBLE);
                moreAddressLink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), DealAddressActivity.class);
                        Bundle args = new Bundle();
                        args.putParcelableArrayList(EXTRA_PLACE, new ArrayList<Place>(places));
                        intent.putExtras(args);
                        startActivity(intent);
                    }
                });
            }
            Place place = places.get(0);
            addressTextView.setText(place.getAddress());
            contactsTextView.setText(place.getPhonesAsString());
            scheduleTextView.setText(place.getScheduleAsHtml());

            contactsTitleTextView.setVisibility(View.VISIBLE);
            locationCardView.setVisibility(View.VISIBLE);

            if (place.getLat() != 0 && place.getLon() != 0) {
                setLatLon(place.getLat(), place.getLon());
                mapLayout.setVisibility(View.VISIBLE);
                createGoogleMap();
            }
        }


    }

    private void formatDate(String dealTimeout) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date timeoutDate = format.parse(dealTimeout);
            SimpleDateFormat updatedFormat = new SimpleDateFormat("d MMMM yyyy");
            dealTimeoutTextView.setText
                    (getString(R.string.deal_timeout_date, updatedFormat.format(timeoutDate)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        LatLng dealLocation = new LatLng(lat, lon);
        this.googleMap.addMarker(new MarkerOptions().position(dealLocation));
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(dealLocation, zoom));
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Intent intent = new Intent(getActivity(), MapsActivity.class);
                intent.putParcelableArrayListExtra(EXTRA_PLACE, new ArrayList<Place>(places));
                startActivity(intent);
            }
        });
    }

    private void createGoogleMap() {
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        mapFragment.getMapAsync(this);
        getChildFragmentManager().beginTransaction().replace(R.id.resized_map, mapFragment).commit();
    }

    private void setLatLon(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }
}
