package com.example.indira.chocotestproject.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DealInfo implements Parcelable {

    @SerializedName("deal_id")
    @Expose
    private int id;

    @SerializedName("title_short")
    @Expose
    private String shortTitle;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("images")
    @Expose
    private List<String> imageUrls;

    @SerializedName("reviews_rate")
    @Expose
    private double reviewsRate;

    @SerializedName("price")
    @Expose
    private int price;

    @SerializedName("economy")
    @Expose
    private int economy;

    @SerializedName("bought")
    @Expose
    private int bought;

    @SerializedName("reviews_number")
    @Expose
    private int reviewsNumber;

    @SerializedName("terms")
    @Expose
    private String conditions;

    @SerializedName("timeout")
    @Expose
    private String dealTimeout;

    @SerializedName("places")
    @Expose
    private List<Place> places;

    @SerializedName("features")
    @Expose
    private String features;

    @SerializedName("questions_number")
    @Expose
    private int questionNumber;

    @SerializedName("partner")
    @Expose
    private Partner partner;

    @SerializedName("rate_statistics")
    @Expose
    private RateStatistics statistics;

    private int spinnerPosition;


    protected DealInfo(Parcel in) {
        id = in.readInt();
        shortTitle = in.readString();
        title = in.readString();
        imageUrls = in.createStringArrayList();
        reviewsRate = in.readDouble();
        price = in.readInt();
        economy = in.readInt();
        bought = in.readInt();
        reviewsNumber = in.readInt();
        conditions = in.readString();
        dealTimeout = in.readString();
        places = in.createTypedArrayList(Place.CREATOR);
        features = in.readString();
        questionNumber = in.readInt();
        partner = in.readParcelable(Partner.class.getClassLoader());
        statistics = in.readParcelable(RateStatistics.class.getClassLoader());

        spinnerPosition = in.readInt();
    }

    public static final Creator<DealInfo> CREATOR = new Creator<DealInfo>() {
        @Override
        public DealInfo createFromParcel(Parcel in) {
            return new DealInfo(in);
        }

        @Override
        public DealInfo[] newArray(int size) {
            return new DealInfo[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getShortTitle() {
        return shortTitle;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public double getReviewsRate() {
        return reviewsRate;
    }

    public int getPrice() {
        return price;
    }

    public int getEconomy() {
        return economy;
    }

    public int getBought() {
        return bought;
    }

    public int getReviewsNumber() {
        return reviewsNumber;
    }

    public String getConditions() {
        return conditions;
    }

    public String getDealTimeout() {
        return dealTimeout;
    }

    public List<Place> getPlaces() {
        return places;
    }

    public String getFeatures() {
        return features;
    }

    public int getQuestionNumber() {
        return questionNumber;
    }

    public Partner getPartner() {
        return partner;
    }

    public RateStatistics getStatistics() {
        return statistics;
    }

    public int getSpinnerPosition() {
        return spinnerPosition;
    }

    public void setSpinnerPosition(int spinnerPosition) {
        this.spinnerPosition = spinnerPosition;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(shortTitle);
        dest.writeString(title);
        dest.writeString(imageUrls.toString());
        dest.writeString(String.valueOf(reviewsRate));
        dest.writeString(String.valueOf(price));
        dest.writeString(String.valueOf(economy));
        dest.writeString(String.valueOf(bought));
        dest.writeString(String.valueOf(reviewsNumber));
        dest.writeString(conditions);
        dest.writeString(dealTimeout);
        dest.writeString(places.toString());
        dest.writeString(features);
        dest.writeInt(questionNumber);
        dest.writeString(partner.toString());
        dest.writeString(statistics.toString());
        dest.writeString(String.valueOf(spinnerPosition));
    }
}
