package com.example.indira.chocotestproject.deal;

public interface IDealPresenter {
    void getDeals(int cityId, int categoryId, String sortingParameter, int page);

    void getSearchDeals(int cityId, String searchParameter, int page);
}
