package com.example.indira.chocotestproject.deal_info.DealComments;

import com.example.indira.chocotestproject.model.DealComment;
import com.example.indira.chocotestproject.model.JsonResponse;
import com.example.indira.chocotestproject.service.ServiceWrapper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DealCommentsPresenterImpl implements IDealCommentsPresenter {

    private DealCommentsFragment view;
    private ServiceWrapper serviceInstance;

    public DealCommentsPresenterImpl(DealCommentsFragment view) {
        this.view = view;
    }


    @Override
    public void getComments(int dealId, int page) {
        serviceInstance = new ServiceWrapper();

        Call<JsonResponse<List<DealComment>>> responseCall =
                serviceInstance.getDealCommentResponse(dealId, page);

        responseCall.enqueue(new Callback<JsonResponse<List<DealComment>>>() {
            @Override
            public void onResponse(Call<JsonResponse<List<DealComment>>> call, Response<JsonResponse<List<DealComment>>> response) {
                if (response.body() != null && response.isSuccessful()) {
                    view.displayComments(response.body().getResponse());
                }
            }

            @Override
            public void onFailure(Call<JsonResponse<List<DealComment>>> call, Throwable t) {
                view.displayError();
            }
        });
    }
}
