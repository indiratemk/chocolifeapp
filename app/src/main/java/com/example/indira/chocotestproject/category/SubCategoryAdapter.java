package com.example.indira.chocotestproject.category;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.indira.chocotestproject.R;
import com.example.indira.chocotestproject.model.Category;

import java.util.List;

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.SubCategoryHolder> {

    private List<Category> subCategories;
    private Context context;
    private OnCategoryClickListener onCategoryClickListener;
    private int selectedSubCategoryIndex = -1;

    public SubCategoryAdapter(Context context, List<Category> subCategories) {
        this.context = context;
        this.subCategories = subCategories;
    }


    @NonNull
    @Override
    public SubCategoryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.subcategory_item, viewGroup, false);
        return new SubCategoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubCategoryHolder subCategoryHolder, int i) {
        Category category = subCategories.get(i);
        subCategoryHolder.subCategoryTitle.setText(category.getTitle());
        subCategoryHolder.subCategoryDealsAmount.setText(String.valueOf(category.getDealsNumber()));


        if (selectedSubCategoryIndex == i) {
            int colorSelected = ContextCompat.getColor(context, R.color.colorPrimaryDark);
            subCategoryHolder.subCategoryTitle.setTextColor(colorSelected);
        } else {
            int colorDefault = ContextCompat.getColor(context, R.color.colorDealText);
            subCategoryHolder.subCategoryTitle.setTextColor(colorDefault);
        }
    }

    @Override
    public int getItemCount() {
        if (subCategories == null) {
            return 0;
        }
        return subCategories.size();
    }

    class SubCategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView subCategoryTitle;
        private TextView subCategoryDealsAmount;

        public SubCategoryHolder(@NonNull View itemView) {
            super(itemView);
            subCategoryTitle = itemView.findViewById(R.id.subcategory_title_text_view);
            subCategoryDealsAmount = itemView.findViewById(R.id.subcategory_deals_amount_text_view);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            selectedSubCategoryIndex = getAdapterPosition();
            Category subCategory = subCategories.get(selectedSubCategoryIndex);
            notifyDataSetChanged();
            onCategoryClickListener.onCategoryClick(subCategory.getId(), subCategory.getDealsNumber());
        }
    }

    public void setOnCategoryClickListener(OnCategoryClickListener onCategoryClickListener) {
        this.onCategoryClickListener = onCategoryClickListener;
    }
}
