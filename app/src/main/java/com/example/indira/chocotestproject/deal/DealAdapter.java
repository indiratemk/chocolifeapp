package com.example.indira.chocotestproject.deal;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.indira.chocotestproject.DealInfoActivity;
import com.example.indira.chocotestproject.DealPageActivity;
import com.example.indira.chocotestproject.R;
import com.example.indira.chocotestproject.model.Deal;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DealAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "DealAdapter";
    public static final String EXTRA_DEAL_ID = "com.example.indira.chocoproject.extra.DEAL_ID";
    public static final String EXTRA_DEAL_LINK = "com.example.indira.chocoproject.extra.DEAL_LINK";

    private List<Deal> deals;
    private Context context;

    OnEndReachedListener onEndReachedListener;

    public DealAdapter(Context context) {
        this.context = context;
//        this.deals  = deals;
        this.deals = new ArrayList<>();
    }

    @Override
    public int getItemViewType(int position) {
        Deal deal = deals.get(position);
        String dealImageKind = deal.getDealImageKind();
        if (dealImageKind.equals("general")) {
            return 0;
        } else {
            return 1;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view;
        switch (viewType) {
            case 0:
                view = LayoutInflater.from(context)
                        .inflate(R.layout.general_deal_layout, viewGroup, false);
                return new GeneralDealHolder(view);
            case 1:
                view = LayoutInflater.from(context)
                        .inflate(R.layout.wide_plate_deal_layout, viewGroup, false);
                return new SpecialDealHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        final Deal deal = deals.get(position);

        if (deals.size() >= 20 && position == deals.size() - 2 && onEndReachedListener != null) {
            onEndReachedListener.onEndReached();
        }
        if (viewHolder instanceof GeneralDealHolder) {
            ((GeneralDealHolder) viewHolder).bindTo(deal);
        } else if (viewHolder instanceof  SpecialDealHolder) {
            ((SpecialDealHolder) viewHolder).bindTo(deal);
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDealInfo(deal);
            }
        });

    }
    @Override
    public int getItemCount() {
        if (deals == null) {
            return 0;
        }
        return deals.size();
    }

    public List<Deal> getDeals() {
        return deals;
    }

    public void setDeals(List<Deal> deals) {
        this.deals = deals;
        notifyDataSetChanged();
    }

    public void clearDealList() {
        this.deals.clear();
        notifyDataSetChanged();
    }

    public void addDeals(List<Deal> deals) {
        this.deals.addAll(deals);
        notifyDataSetChanged();
    }

    private void showDealInfo(Deal deal) {
        Intent intent;
        switch (deal.getDealOpenAction()) {
            case "general":
                intent = new Intent(context, DealInfoActivity.class);
                intent.putExtra(EXTRA_DEAL_ID, deal.getDealId());
                context.startActivity(intent);
                Log.d(TAG, "onBindViewHolder: general");
                break;
            case "webview":
                Log.d(TAG, "onBindViewHolder: webview");
                intent = new Intent(context, DealPageActivity.class);
                intent.putExtra(EXTRA_DEAL_LINK, deal.getDealLink());
                context.startActivity(intent);
                break;
            case "browser":
                Log.d(TAG, "onBindViewHolder: browser");
                break;
        }
    }

    public interface OnEndReachedListener {
        void onEndReached();
    }

    public void setOnEndReachedListener(OnEndReachedListener onEndReachedListener) {
        this.onEndReachedListener = onEndReachedListener;
    }
}
