package com.example.indira.chocotestproject.deal_info.DealReviews;

import com.example.indira.chocotestproject.model.DealReview;

import java.util.List;

public interface IDealReviewsView {
    void displayReviews(List<DealReview> dealReviews);

    void showError();
}
