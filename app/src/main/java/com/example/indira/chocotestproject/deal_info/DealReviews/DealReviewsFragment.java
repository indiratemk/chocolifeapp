package com.example.indira.chocotestproject.deal_info.DealReviews;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.indira.chocotestproject.R;
import com.example.indira.chocotestproject.deal.DealAdapter;
import com.example.indira.chocotestproject.model.DealInfo;
import com.example.indira.chocotestproject.model.DealReview;
import com.example.indira.chocotestproject.util.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DealReviewsFragment extends Fragment implements IDealReviewsView {
    private static final String TAG = "DealReviewsFragment";
    private static final String ARG_DEAL_INFO = "deal_info";

    private DealInfo dealInfo;
    private RecyclerView reviewsRecyclerView;
    private DealReviewsAdapter reviewsAdapter;
    private int currentMark = 0;

    private List<DealReview> allDealReviews = new ArrayList<>();

    private int dealId;
    private int pageAmount;
    private int reviewsAmount;
    private int current_page = 1;

    private IDealReviewsPresenter dealReviewsPresenter;

    public static DealReviewsFragment getInstance(DealInfo dealInfo) {
        DealReviewsFragment fragment = new DealReviewsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_DEAL_INFO, dealInfo);
        fragment.setArguments(bundle);
        Log.d(TAG, "getInstance: reviews");
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: reviews");
        View view = inflater.inflate(R.layout.fragment_deal_reviews, container, false);
        reviewsRecyclerView = view.findViewById(R.id.reviews_recycler_view);
        dealReviewsPresenter = new DealReviewsPresenterImpl(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            dealInfo = bundle.getParcelable(ARG_DEAL_INFO);
            dealId = dealInfo.getId();
            reviewsAmount = dealInfo.getReviewsNumber();
            updatePageAmount();
            dealInfo.setSpinnerPosition(0);
            dealReviewsPresenter.getReviews(dealId, Constant.INITIAL_PAGE, currentMark);
        }
        return view;
    }


    @Override
    public void displayReviews(final List<DealReview> dealReviews) {
        if (allDealReviews.isEmpty()) {
            allDealReviews = dealReviews;
            reviewsAdapter =
                    new DealReviewsAdapter(getActivity(), dealReviews, dealInfo);
            reviewsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            reviewsRecyclerView.setAdapter(reviewsAdapter);
        } else {
            if (allDealReviews.size() == reviewsAmount
                    || pageAmount < current_page) {
                current_page = Constant.INITIAL_PAGE;
                return;
            }
            allDealReviews.addAll(dealReviews);
            reviewsAdapter.notifyDataSetChanged();
        }
        reviewsAdapter.setOnEndReachedListener(new DealAdapter.OnEndReachedListener() {
            @Override
            public void onEndReached() {
                current_page += 1;
                dealReviewsPresenter.getReviews(dealId, current_page, currentMark);
            }
        });
        reviewsAdapter.setOnSortBySelectListener(new DealReviewsAdapter.OnSortBySelectListener() {
            @Override
            public void onSortBySelect(int userMark) {
                if (currentMark != userMark) {
                    currentMark = userMark;
                    allDealReviews.clear();
                    switch (currentMark) {
                        case 5:
                            reviewsAmount = dealInfo.getStatistics().getFiveStarsCount();
                            break;
                        case 4:
                            reviewsAmount = dealInfo.getStatistics().getFourStarsCount();
                            break;
                        case 3:
                            reviewsAmount = dealInfo.getStatistics().getThreeStarsCount();
                            break;
                        case 2:
                            reviewsAmount = dealInfo.getStatistics().getTwoStarsCount();
                            break;
                        case 1:
                            reviewsAmount = dealInfo.getStatistics().getOneStarCount();
                            break;
                        case 0:
                            reviewsAmount = dealInfo.getStatistics().getReviewsNumber();
                            break;
                    }
                    Log.d(TAG, "onSortBySelect: reviews amount: " + reviewsAmount);
                    updatePageAmount();
                    Log.d(TAG, "onSortBySelect: pages amount: " + pageAmount);
                    dealReviewsPresenter.getReviews(dealId, Constant.INITIAL_PAGE, userMark);
                }
            }
        });
    }

    @Override
    public void showError() {
        Toast.makeText(getActivity(),
                "Пожалуйста проверьте Интернет соединение", Toast.LENGTH_SHORT).show();
    }

    public void updatePageAmount() {
        pageAmount = reviewsAmount/ Constant.AMOUNT_ITEMS_IN_PAGE;
        if (reviewsAmount/(double) Constant.AMOUNT_ITEMS_IN_PAGE > pageAmount) {
            pageAmount += 1;
        }
    }
}
