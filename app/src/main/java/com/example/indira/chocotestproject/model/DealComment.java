package com.example.indira.chocotestproject.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DealComment{

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("text")
    @Expose
    private String commentText;

    @SerializedName("time")
    @Expose
    private String commentTime;

    @SerializedName("user")
    @Expose
    private User user;

    @SerializedName("answer")
    @Expose
    private Answer answer;


    public int getId() {
        return id;
    }

    public String getCommentText() {
        return commentText;
    }

    public String getCommentTime() {
        return commentTime;
    }

    public User getUser() {
        return user;
    }

    public Answer getAnswer() {
        return answer;
    }
}
