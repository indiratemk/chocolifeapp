package com.example.indira.chocotestproject.deal_info.DealComments;

import com.example.indira.chocotestproject.model.DealComment;

import java.util.List;

public interface IDealCommentsView {
    void displayComments(List<DealComment> dealComments);

    void displayError();
}
