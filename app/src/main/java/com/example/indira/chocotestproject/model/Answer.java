package com.example.indira.chocotestproject.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Answer {

    @SerializedName("user_answer")
    @Expose
    private String answerText;

    @SerializedName("user")
    @Expose
    private User user;

    public String getAnswerText() {
        return answerText;
    }

    public User getUser() {
        return user;
    }
}
