package com.example.indira.chocotestproject.deal_info;

import com.example.indira.chocotestproject.model.DealComment;
import com.example.indira.chocotestproject.model.DealInfo;

import java.util.List;

public interface IDealInfoView {
    void displayDealInfo(DealInfo dealInfo);

    void showError();
}
